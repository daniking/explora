struct VertexIn {
    @location(0) pos: vec3<f32>,
    @location(1) tex_pos: vec2<f32>,
}

struct VertexOut {
    @builtin(position) vertex_pos: vec4<f32>,
    @location(0) tex_pos: vec2<f32>,
}

@vertex
fn vs_main(in: VertexIn) -> VertexOut  {
    var out: VertexOut;
    out.vertex_pos = vec4<f32>(in.pos, 1.0);
    out.tex_pos = in.tex_pos;
    return out;
}

@group(0) @binding(0)
var texture: texture_2d<f32>;
@group(0) @binding(1)
var tex_sampler: sampler;

@fragment
fn fs_main(in: VertexOut) -> @location(0) vec4<f32> {
    return textureSample(texture, tex_sampler, in.tex_pos);
}