pub mod error;
pub mod pipelines;
pub mod texture;

use std::path::Path;

use error::RenderError;
use image::{GenericImage, GenericImageView};
use logger::info;
use pipelines::terrain::TerrainPipeline;
use pollster::FutureExt;
use texture::Texture;
use wgpu::util::DeviceExt;

use crate::pipelines::terrain::TerrainVertex;

#[derive(Clone)]
pub struct Dimensions {
    pub width: u32,
    pub height: u32,
}

impl Dimensions {
    pub fn new(width: u32, height: u32) -> Self {
        Self { width, height }
    }
}
pub struct Renderer {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    terrain: TerrainPipeline,
    square_test_buffer: wgpu::Buffer,
    dimensions: Dimensions,
    test_tex: Texture,
    test_tex_bind_group: wgpu::BindGroup,
    index_buf: wgpu::Buffer,
}

impl Renderer {
    pub fn new<W>(window: &W, dim: Dimensions) -> Result<Self, RenderError>
    where
        W: raw_window_handle::HasRawWindowHandle + raw_window_handle::HasRawDisplayHandle,
    {
        let backends = wgpu::Backends::all();
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends,
            dx12_shader_compiler: wgpu::Dx12Compiler::default(),
        });
        logger::info!("Using: {:?}", backends);

        let surface = unsafe { instance.create_surface(window) }.unwrap();

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .block_on()
            .ok_or(RenderError::AdapterNotFound)?;

        logger::info!("Using adapter: {:?}", adapter.get_info());

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty() | wgpu::Features::POLYGON_MODE_LINE,
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None, // Trace path
            )
            .block_on()?;

        let sfc_caps = surface.get_capabilities(&adapter);

        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: sfc_caps.formats[0],
            width: dim.width,
            height: dim.height,
            present_mode: sfc_caps.present_modes[0],
            alpha_mode: sfc_caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);
        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("terrain"),
            source: wgpu::ShaderSource::Wgsl(include_str!("../shaders/terrain.wgsl").into()),
        });

        let square_mesh = vec![
            // counter clockwise
            TerrainVertex { pos: [-0.5, -0.5, 0.0], uv: [0.0, 0.0] },
            TerrainVertex { pos: [0.5, -0.5, 0.0], uv: [1.0, 0.0] },
            TerrainVertex { pos: [0.5, 0.5, 0.0], uv: [1.0, 1.0] },
            TerrainVertex { pos: [-0.5, 0.5, 0.0], uv: [0.0, 1.0] },
        ];
        let indices: [u16; 6] = [0, 1, 2, 2, 3, 0];
        let square_test_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Square test"),
            contents: bytemuck::cast_slice(&square_mesh),
            usage: wgpu::BufferUsages::VERTEX,
        });
        let square_index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Square test index"),
            contents: bytemuck::cast_slice(&indices),
            usage: wgpu::BufferUsages::INDEX,
        });
        let image = image::load_from_memory(include_bytes!("../textures/dirt.png"))
            .expect("Failed to load image");
        let test_texture = Texture::new(&device, &queue, image);

        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("Test texture"),
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
            });
        let texture_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Test texture"),
            layout: &texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&test_texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&test_texture.sampler),
                },
            ],
        });
        let terrain = pipelines::terrain::TerrainPipeline::new(
            &device,
            &config,
            shader,
            &[&texture_bind_group_layout],
        );  
        
        Texture::pack_textures("render/blocks", 16);
        let this = Self {
            surface,
            device,
            queue,
            config,
            terrain,
            square_test_buffer,
            dimensions: dim,
            test_tex: test_texture,
            test_tex_bind_group: texture_bind_group,
            index_buf: square_index_buffer,
        };
        Ok(this)
    }

    pub fn render(&mut self) -> Result<(), RenderError> {
        let sfc_tex = match self.surface.get_current_texture() {
            Ok(t) => t,
            Err(wgpu::SurfaceError::Timeout) => return Ok(()),
            Err(error @ wgpu::SurfaceError::Outdated) => {
                logger::warn!("Surface outdated: {:?}", error);
                return Ok(());
            },
            Err(error @ wgpu::SurfaceError::Lost) => {
                logger::warn!("Surface lost: {:?}", error);
                self.resize(self.dimensions.clone());
                return Ok(());
            },
            Err(wgpu::SurfaceError::OutOfMemory) => return Err(RenderError::OutOfMemory),
        };
        let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });
        let view = sfc_tex.texture.create_view(&wgpu::TextureViewDescriptor::default());
        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("Main Pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color { r: 0.1, g: 0.3, b: 0.6, a: 1.0 }),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });
        render_pass.set_pipeline(&self.terrain.0);
        render_pass.set_bind_group(0, &self.test_tex_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.square_test_buffer.slice(..));
        render_pass.set_index_buffer(self.index_buf.slice(..), wgpu::IndexFormat::Uint16);
        render_pass.draw_indexed(0..6, 0, 0..1);
        std::mem::drop(render_pass);
        self.queue.submit(std::iter::once(encoder.finish()));
        sfc_tex.present();
        Ok(())
    }

    pub fn resize(&mut self, dim: Dimensions) {
        if dim.width == 0 || dim.height == 0 {
            // Resize with 0 width and height is used by winit to signal a minimize event on Windows.
            // Refer to: https://github.com/rust-windowing/winit/issues/208
            // This solves an issue where the app would panic when minimizing on Windows.
            return;
        }
        self.config.width = dim.width;
        self.config.height = dim.height;
        self.surface.configure(&self.device, &self.config);
    }
}
