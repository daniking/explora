use std::path::Path;

use image::{GenericImage, GenericImageView};

pub struct Texture {
    pub handle: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
}

impl Texture {
    pub fn new(device: &wgpu::Device, queue: &wgpu::Queue, image: image::DynamicImage) -> Self {
        // Handle errors for unsupported image formats
        match image {
            image::DynamicImage::ImageLumaA8(_) => {
                panic!("Image format not supported: ImageLumaA8")
            },
            image::DynamicImage::ImageLuma16(_) => {
                panic!("Image format not supported: ImageLuma16")
            },
            image::DynamicImage::ImageLumaA16(_) => {
                panic!("Image format not supported: ImageLumaA16")
            },
            image::DynamicImage::ImageRgb16(_) => panic!("Image format not supported: ImageRgb16"),
            image::DynamicImage::ImageRgba16(_) => {
                panic!("Image format not supported: ImageRgba16")
            },
            _ => (),
        };

        let data = &image.to_rgba8();

        let size = wgpu::Extent3d {
            width: image.width(),
            height: image.height(),
            depth_or_array_layers: 1,
        };

        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            view_formats: &[],
        });

        queue.write_texture(
            wgpu::ImageCopyTexture {
                texture: &texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            data,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * image.width()),
                rows_per_image: Some(image.height()),
            },
            size,
        );

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Self { handle: texture, view, sampler }
    }

    pub fn pack_textures<P: AsRef<Path>>(path: P, res: u32)  {
        let Ok(dir_iter) = std::fs::read_dir(path) else {
        return;
    };
        let entry_iter = dir_iter
            .into_iter()
            .flat_map(|res| match res {
                Ok(t) => Some(t),
                Err(e) => {
                    logger::debug!("Skipping over file: {:?}", e);
                    None
                },
            })
            .filter(|entry| entry.path().extension().and_then(|ext| ext.to_str()) == Some("png"))
            .map(|entry| image::open(entry.path()).expect("Failed to open image"))
            .filter(|img| (res, res) == img.dimensions());

        let w = 1024;
        let h = 1024;
        let mut atlas_buffer = image::RgbaImage::new(w, h);

        for (i, entry) in entry_iter.enumerate() {
            logger::debug!("Packing texture: {:?}", entry);

            let res = res as usize;
            let atlas_x = (i * res) as f32 % w as f32;
            let atlas_y = {
                let y = (i * res) as f32 / w as f32;
                y.floor() * res as f32
            };
            atlas_buffer.copy_from(&entry, atlas_x as u32, atlas_y as u32).unwrap();
        }
        atlas_buffer.save("packed.png").expect("Failed to save image");
    }
}
