pub mod terrain;

/// Represents a Vertex.
pub trait Vertex: Copy + bytemuck::Pod {
    const STRIDE: wgpu::BufferAddress = std::mem::size_of::<Self>() as wgpu::BufferAddress;
    const INDEX_FORMAT: Option<wgpu::IndexFormat>;

    fn desc<'a>() -> wgpu::VertexBufferLayout<'a>;
}
