#[derive(Debug)]
pub enum RenderError {
    AdapterNotFound,
    DeviceRequestFailed,
    OutOfMemory,
}
impl From<wgpu::RequestDeviceError> for RenderError {
    fn from(e: wgpu::RequestDeviceError) -> Self {
        Self::DeviceRequestFailed
    }
}
