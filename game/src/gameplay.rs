use client::Client;

use crate::{
    scene::{
        Scene,
        SceneCommand::{self, Continue},
    },
    window::Event,
};

pub struct Gameplay {
    client: Client,
}

impl Gameplay {
    pub fn new(client: Client) -> Self {
        Self { client }
    }
}

impl Scene for Gameplay {
    fn init(&mut self) {}

    fn tick(&mut self, events: Vec<Event>) -> SceneCommand {
        for entry in events {
            match entry {
                Event::Close => return SceneCommand::Shutdown,
                Event::Resize(_) => {},
                Event::CursorMove(_) => {},
                Event::Input(_, _) => {},
            }
        }
        Continue
    }

    fn render(&self) {}
}
