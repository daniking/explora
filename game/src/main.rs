use client::Client;
use explora_game::{gameplay::Gameplay, scene::SceneManager, window, Game};

fn main() {
    logger::init();

    let (event_loop, window) = match window::Window::new() {
        Ok(t) => t,
        Err(_) => {
            panic!("Failed to create window");
        },
    };

    let mut game = Game { window };
    let mut scene_manager = SceneManager::with(Box::new(Gameplay::new(Client::new())));
    scene_manager.init_active();

    event_loop.run(move |event, _, flow| {
        flow.set_poll();
        match event {
            winit::event::Event::WindowEvent { window_id, event } => {
                if game.window.id() == window_id {
                    game.window.capture_window_event(event);
                }
            },
            winit::event::Event::MainEventsCleared => {
                let shutdown = scene_manager.update(|| game.window.take_events());
                if shutdown {
                    flow.set_exit();
                }
                game.window.renderer_mut().render().expect("Unrecoverable render error occurred");
            },
            winit::event::Event::DeviceEvent { event, .. } => {
                game.window.capture_device_event(event);
            },
            _ => (),
        }
    });
}
