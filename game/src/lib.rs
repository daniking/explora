use window::Window;

pub mod gameplay;
pub mod scene;
pub mod window;

pub struct Game {
    pub window: Window,
}
