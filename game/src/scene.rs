//! This module contains the API for managing the game's scenes.
//!
//! A `Scene` can be seen as an individual state of the game. For example, a title screen,
//! pause menu, or the game session itself. a `Scene` trait should be implemented
//! for each one of these states.
//!
//! A stack of `Scene`s is maintained, and the topmost scene is the one that is currently
//! active. The active scene is ticked and rendered each frame.
//!
//! The `SceneCommand` enum is used to control the scene stack. It is returned by the
//! `tick` method of the active scene.

use crate::window::Event;

/// Represents a Scene.
pub trait Scene {
    /// Called when the scene is first pushed onto the stack.
    fn init(&mut self);

    /// Ticks the scene. It is used to update the scene's state.
    fn tick(&mut self, events: Vec<Event>) -> SceneCommand;

    /// Draws the scene.
    fn render(&self);
}

/// Represents the action to take after a scene has been ticked.
pub enum SceneCommand {
    /// Push a new scene onto the stack.
    Push(Box<dyn Scene>),
    /// Pop the current scene off the stack.
    Pop,
    /// Replace the current scene with a new scene.
    Replace(Box<dyn Scene>),
    /// Keep ticking the current scene.
    Continue,
    /// Exit the game.
    Shutdown,
}

/// Manages the game's scenes.
///
/// A stack of `Scene`s is maintained, and the topmost scene is the one that is currently
/// active. The active scene is ticked and rendered each frame.
pub struct SceneManager {
    scenes: Vec<Box<dyn Scene>>,
}

impl SceneManager {
    /// Creates a new `SceneManager` with the given scene.
    pub fn with(scene: Box<dyn Scene>) -> Self {
        Self { scenes: vec![scene] }
    }
    /// Initializes the active scene.
    pub fn init_active(&mut self) {
        self.scenes.last_mut().map(|scene| scene.init());
    }

    /// Pushes a new scene onto the stack.
    pub fn push(&mut self, scene: Box<dyn Scene>) {
        self.scenes.push(scene);
    }
    /// Pops the current scene off the stack.
    pub fn pop(&mut self) {
        self.scenes.pop();
    }

    /// Gets a mutable reference to the active scene.
    pub fn active(&mut self) -> Option<&mut Box<dyn Scene>> {
        self.scenes.last_mut()
    }

    /// Updates the active scene.
    ///
    /// Returns whether the scene stack is empty i.e the game should exit.
    pub fn update(&mut self, events: impl FnOnce() -> Vec<Event>) -> bool {
        let Some(scene) = self.active() else {
            return true;
        };
        match scene.tick(events()) {
            SceneCommand::Push(_) => {},
            SceneCommand::Pop => {},
            SceneCommand::Replace(_) => {},
            SceneCommand::Continue => (),
            SceneCommand::Shutdown => {
                logger::debug!("Shutting down scenes");
                self.scenes.clear();
            },
        }
        self.active().is_none()
    }
}
