use common::math::Vec2;
use render::{Dimensions, Renderer};

type EventLoop = winit::event_loop::EventLoop<()>;
type Key = winit::event::VirtualKeyCode;

/// Represents an event that can occur on a [Window].
/// It is a subset of the ones defined in [winit::event::WindowEvent] along with some custom events.
#[derive(Debug, Clone)]
pub enum Event {
    /// The window has been requested to close, (e.g. by the user clicking the close button).
    Close,
    /// The window has been resized.
    Resize(Vec2<u32>),
    /// The cursor has been moved.
    CursorMove(Vec2<u32>),
    /// A key has been pressed. The bool indicates whether the key was pressed or released.
    Input(GameInput, bool),
}

#[derive(Debug, Clone)]
pub enum InputType {
    Key(winit::event::VirtualKeyCode),
    MouseButton(winit::event::MouseButton),
    ScanCode(winit::event::ScanCode),
}

#[derive(Debug, Clone)]
pub enum GameInput {
    MoveForward,
    MoveBackward,
    MoveLeft,
    MoveRight,
}

/// Represents a window.
pub struct Window {
    /// The winit window implementation.
    winit_impl: winit::window::Window,
    /// A list of events that have been captured since the last call to [`Window::take_events`].
    events: Vec<Event>,
    renderer: Renderer,
}

impl Window {
    pub fn new() -> Result<(EventLoop, Self), ()> {
        let event_loop = EventLoop::new();
        let builder = winit::window::WindowBuilder::new()
            .with_title("Explora")
            .with_inner_size(winit::dpi::LogicalSize::new(800, 600));
        let window = builder.build(&event_loop).expect("Failed to create window");
        let size = window.inner_size();
        let renderer =
            render::Renderer::new(&window, Dimensions::new(size.width, size.height)).unwrap();
        let this = Self { winit_impl: window, events: Vec::new(), renderer };

        Ok((event_loop, this))
    }

    pub fn capture_window_event(&mut self, event: winit::event::WindowEvent) {
        match event {
            winit::event::WindowEvent::CloseRequested => self.events.push(Event::Close),
            winit::event::WindowEvent::Resized(size) => {
                self.events.push(Event::Resize(Vec2::new(size.width, size.height)));
                self.renderer.resize(Dimensions::new(size.width, size.height));
            },
            winit::event::WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                self.events
                    .push(Event::Resize(Vec2::new(new_inner_size.width, new_inner_size.height)));
                self.renderer.resize(Dimensions::new(new_inner_size.width, new_inner_size.height));
            },
            winit::event::WindowEvent::KeyboardInput { input, .. } => {
                let key = match input.virtual_keycode {
                    Some(key) => InputType::Key(key),
                    None => InputType::ScanCode(input.scancode),
                };
                if let Some(game_input) = Self::map_input(key) {
                    self.events.push(Event::Input(
                        game_input,
                        input.state == winit::event::ElementState::Pressed,
                    ));
                }
            },
            winit::event::WindowEvent::MouseInput { state, button, .. } => {
                if let Some(game_input) = Self::map_input(InputType::MouseButton(button)) {
                    self.events.push(Event::Input(
                        game_input,
                        state == winit::event::ElementState::Pressed,
                    ));
                }
            },
            _ => (),
        }
    }

    pub fn capture_device_event(&mut self, event: winit::event::DeviceEvent) {
        if let winit::event::DeviceEvent::MouseMotion { delta: (dx, dy) } = event {
            self.events.push(Event::CursorMove(Vec2::new(dx as u32, dy as u32)));
            // TODO: handle sensitivity and mouse inversion
        }
    }

    /// Set grabbing mode on the cursor preventing it from leaving the window.
    /// Aditionally, the cursor visibility is the opposite of the grab value.
    /// (i.e. if grab is true, the cursor is hidden, and vice versa)
    pub fn grab_cursor(&mut self, grab: bool) {
        self.winit_impl.set_cursor_visible(!grab);
        let mode = if grab {
            winit::window::CursorGrabMode::Locked
        } else {
            winit::window::CursorGrabMode::None
        };
        if let Err(e) = self.winit_impl.set_cursor_grab(mode) {
            logger::warn!("Could not grab cursor in {:?} mode ({})", mode, e);
        }
    }

    pub fn take_events(&mut self) -> Vec<Event> {
        std::mem::take(&mut self.events)
    }

    pub fn id(&self) -> winit::window::WindowId {
        self.winit_impl.id()
    }

    pub fn scale_factor(&self) -> f64 {
        self.winit_impl.scale_factor()
    }

    pub fn dimensions(&self) -> Vec2<u32> {
        let size = self.winit_impl.inner_size();
        Vec2::new(size.width, size.height)
    }

    pub fn renderer(&self) -> &Renderer {
        &self.renderer
    }

    pub fn renderer_mut(&mut self) -> &mut Renderer {
        &mut self.renderer
    }

    fn map_input(key: InputType) -> Option<GameInput> {
        // TODO: map mouse buttons
        let input = match key {
            InputType::Key(Key::W) => GameInput::MoveForward,
            InputType::Key(Key::S) => GameInput::MoveBackward,
            InputType::Key(Key::A) => GameInput::MoveLeft,
            InputType::Key(Key::D) => GameInput::MoveRight,
            _ => return None,
        };
        Some(input)
    }
}
