//! This module is used for logging events to the console / file.
//!
//! There are two features available:
//! * `producer`: This feature is used to *emit* logs. Here you can use
//!    the `error!`, `warn!`, `info!`, `debug!` and `trace!` macros.
//! * `consumer`: This feature is used to *consume* logs by outputting them to
//!    standard output, file, etc.
//!
//! Tracing level use:
//!
//! * `Error`: A problem that could not be handled has occured.
//! E.g: Connection lost, resource not found, etc.
//!
//! * `Warn`: An event which *might* cause problems has occured.
//! It is not an error, and it's not stopping the program, but it's
//! something might be worth investigating.
//! E.g: Could not update entity, connection timeout, etc.
//!
//! * `Info`: An event which is worth nothing and / or might be useful
//! to the user. E.g a player has joined, your settings have been saved, etc.
//!
//! * `Debug`: An event which is useful for debugging. Everything here is used
//! for debugging purposes for developers. It is worth noting that if this event
//! is happening too often, it might be worth moving it to `Trace` level.
//! E.g: The entity 1 has jumped over a block with x material, the player has
//! reached the highest y axis, etc.
//!
//! * `Trace`: An event that is repeated very often. This level is the only one
//! that is allowed to spam to the console / file. E.g: The player has moved,
//! a packet has been received, a new tick has started, etc.

#[cfg(feature = "producer")]
pub use tracing::{debug, error, info, trace, warn};

#[cfg(feature = "consumer")]
const LOG_ENV: &str = "RUST_LOG";

/// Initializes the logger.
///
/// The logging level is set to `INFO` by default. If you want to change it,
/// you can use the `RUST_LOG` environment variable.
///
/// For example, if you want to set the global logging level to `DEBUG`, you can use
/// the following command:
/// `RUST_LOG="info" cargo run`
pub fn init() {
    use tracing::metadata::LevelFilter;

    let mut filter =
        tracing_subscriber::EnvFilter::default().add_directive(LevelFilter::INFO.into());

    // TODO: add default directives

    // Here we manually parse the `RUST_LOG` environment variable, to notify whenever there is an invalid directive.
    match std::env::var(LOG_ENV) {
        Ok(str) => {
            for entry in str.split(",") {
                match entry.parse() {
                    Ok(directive) => filter = filter.add_directive(directive),
                    Err(_) => eprintln!("[WARN]: Invalid log directive: {}", entry),
                }
            }
        },
        Err(std::env::VarError::NotUnicode(ostr)) => {
            eprintln!("[WARN]: Invalid unicode in log directive: {:?}", ostr)
        },
        // If the variable is not set, we just ignore it.
        Err(_) => (),
    }

    tracing_subscriber::fmt().with_env_filter(filter).init();

    let level = LevelFilter::current().to_string().to_uppercase();
    tracing::info!("Logging level: {}", level);
}
