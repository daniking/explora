pub mod math;

use std::time::Duration;

use apecs::{anyhow::Ok, World};

/// The `RunMode` enum represents what mode the game is running in.
#[derive(Debug, Clone, Copy)]
pub enum RunMode {
    /// The game is running in singleplayer mode. (i.e both client and server)
    Singleplayer,
    /// The game is running in client mode (i.e the client is connected to a remote server)
    Client,
    /// The game is running in server mode (i.e this is the server)
    Server,
}

/// The `State` represents the game state that is shared between
/// the server and the clients. For example, this could include
/// a list of loaded chunks, entities, etc.
pub struct State {
    ecs: World,
}

impl State {
    /// Create a new `State` instance for the given `RunMode`.
    pub fn new(mode: RunMode) -> Self {
        Self { ecs: Self::setup(mode).expect("Failed to setup ECS") }
    }

    /// Create a new `State` with client configuration.
    pub fn client() -> Self {
        Self::new(RunMode::Client)
    }

    /// Create a new `State` with server configuration.
    pub fn server() -> Self {
        Self::new(RunMode::Server)
    }

    /// Execute a single state tick.
    ///
    /// The state will advance the game state by the given duration.
    pub fn tick(&mut self, _: Duration) {}

    /// Returns a reference to the ECS world.
    pub fn ecs(&mut self) -> &apecs::World {
        &self.ecs
    }

    /// Returns a mutable reference to the ECS world.
    pub fn ecs_mut(&mut self) -> &mut apecs::World {
        &mut self.ecs
    }

    /// Setup the ECS World.
    fn setup(run_mode: RunMode) -> apecs::anyhow::Result<World> {
        let mut ecs = apecs::World::default();
        ecs.with_resource(run_mode)?;
        Ok(ecs)
    }
}
