use std::time::Duration;

use common::State;

/// Represents a `Client`.
#[allow(dead_code)]
pub struct Client {
    state: State,
}

impl Client {
    /// Create a `Client` instance.
    pub fn new() -> Self {
        Self { state: State::client() }
    }

    /// Execute a single client tick
    ///
    /// The client will process game logic and update the game state
    /// by the given duration.
    pub fn tick(&mut self, _: Duration) {}
}
