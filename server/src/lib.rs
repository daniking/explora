use common::State;
use std::time::Duration;

/// Represents a `Server`.
#[allow(dead_code)]
pub struct Server {
    state: State,
}

impl Server {
    /// Create a `Server` instance.
    pub fn new() -> Self {
        Self { state: State::server() }
    }

    /// Execute a single server tick
    ///
    /// The server will process game logic and update the game state
    /// by the given duration.
    pub fn tick(&mut self, _: Duration) {
        // By the end of this tick the server should notify to all clients
        // the game state updates.
    }
}
