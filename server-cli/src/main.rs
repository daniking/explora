use server::Server;

fn main() {
    logger::init();
    logger::info!("Starting server...");

    let mut server = Server::new();

    loop {
        server.tick(std::time::Duration::ZERO);
    }
}
